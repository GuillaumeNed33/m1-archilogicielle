package legacy;

public interface Pricing extends Cloneable {
    double calcAmount(int daysRented);
    int calclFrequentRenterPoints(int daysRented);
    Pricing clone();
}
