package legacy;

import java.util.ArrayList;
import java.util.List;

public class Customer {
    private String _name;
    private List<Rental> _rentals;

    public Customer(String name) {
        _name=name;
        _rentals = new ArrayList<>();
    }

    public void addRental(Rental rental) {
        _rentals.add(rental);
    }

    public String getName()	{
        return _name;
    }

    public String statement() {
        String result = "Rental Record for "+ getName()+"\n";
        for(Rental each : _rentals) {
            result += "\t" + each.getMovie().getTitle()+"\t"+ String.valueOf(each.calcAmount()) +" \n";
        }
        result += "Amount owned is " + String.valueOf(getTotalAmount()) +"\n";
        result += "You earned " + String.valueOf(getFrequentRenterPoints()) + " frequent renter points";
        return result;
    }

    private double getTotalAmount() {
        double total = 0;
        for(Rental each : _rentals) {
           total += each.calcAmount();
        }
        return total;
    }

    private int getFrequentRenterPoints() {
        int points = 0;
        for(Rental each : _rentals) {
            points += each.calcRenterPoints();
        }
        return points;
    }
}
