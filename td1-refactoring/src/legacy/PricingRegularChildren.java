package legacy;

public class PricingRegularChildren implements Pricing {
    private double _basePrice;
    private double _daysBundle;
    private double _priceOutOfBundle;

    public PricingRegularChildren(double _basePrice, double _daysBundle, double _priceOutOfBundle) {
        this._basePrice = _basePrice;
        this._daysBundle = _daysBundle;
        this._priceOutOfBundle = _priceOutOfBundle;
    }

    @Override
    public Pricing clone() {
        Pricing o = null;
        try {
            // On récupère l'instance à renvoyer par l'appel de la
            // méthode super.clone()
            o = (Pricing)super.clone();
        } catch(CloneNotSupportedException cnse) {
            // Ne devrait jamais arriver car nous implémentons
            // l'interface Cloneable
            cnse.printStackTrace(System.err);
        }
        // on renvoie le clone
        return o;    }

    @Override
    public double calcAmount(int daysRented) {
        double thisAmount = _basePrice;
        if(daysRented > _daysBundle)
            thisAmount += (daysRented-_daysBundle)*_priceOutOfBundle;
        return thisAmount;
    }

    @Override
    public int calclFrequentRenterPoints(int daysRented) {
        return 1;
    }
}
