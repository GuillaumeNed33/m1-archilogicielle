package legacy;

/**
 * Classe d'un Film
 * Données : le nom du film, le prix du film
 */
public class Movie {
    /*public static final int CHILDRENS = 2;
    public static final int REGULAR = 0;
    public static final int NEW_RELEASE = 1;

    public static final double BASE_PRICE_CHILDREN = 1.5;
    public static final double BASE_PRICE_REGULAR = 2;
    public static final double BASE_PRICE_NEW_RELEASE = 3;

    public static final double DAYS_FORFAIT_CHILDREN = 3;
    public static final double DAYS_FORFAIT_REGULAR = 2;
    public static final double DAYS_FORFAIT_NEW_RELEASE = 0;

    public static final double PRICE_FORFAIT_OUT_CHILDREN = 1.5;
    public static final double PRICE_FORFAIT_OUT_REGULAR = 1.5;
    public static final double PRICE_FORFAIT_OUT_NEW_RELEASE = 3;*/

    //CHILDREN
    public static final Pricing CHILDRENS = new PricingRegularChildren(1.5, 3, 1.5);
    //REGULAR
    public static final Pricing REGULAR = new PricingRegularChildren(2, 2, 1.5);
    //NEW RELEASE
    public static final Pricing NEW_RELEASE = new PricingNewRelease(3);

    private String _title;
    private Pricing _priceCode;

    public Movie(String title,Pricing priceCode) {
        _title=title;
        _priceCode=priceCode;
    }

    public Pricing getPriceCode() {
        return _priceCode.clone();
    }

    public void setPriceCode(Pricing priceCode) {
        _priceCode=priceCode;
    }

    public String getTitle() {
        return _title;
    }
}
