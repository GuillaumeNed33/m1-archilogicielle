package legacy;

public class PricingNewRelease extends PricingRegularChildren {

    public PricingNewRelease(double _basePrice) {
        super(0, 0, _basePrice);
    }

    @Override
    public int calclFrequentRenterPoints(int daysRented) {
        return (daysRented > 1) ? 2 : 1;
    }
}
