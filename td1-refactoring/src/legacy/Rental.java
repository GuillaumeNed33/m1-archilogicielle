package legacy;

/**
 * Classe des locations enregistrant le film loué et la durée en jours de location
 * Service proposé : connaitre le film loué / connaitre la durée de location
 */
public class Rental {
    private Movie _movie;
    private int _daysRented;
    private Pricing _price;

    public Rental(Movie movie, int daysRented) {
        _movie=movie;
        _daysRented=daysRented;
        _price = movie.getPriceCode();
    }

    private int getDaysRented() {
        return _daysRented;
    }

    public Movie getMovie() {
        return _movie;
    }

    public double calcAmount() {
        return _price.calcAmount(_daysRented);
    }

    public int calcRenterPoints() {
        return _price.calclFrequentRenterPoints(_daysRented);
    }
}
