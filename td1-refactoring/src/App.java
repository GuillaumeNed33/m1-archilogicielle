import model.*;
import legacy.*;
import org.junit.Test;

import static org.junit.Assert.*;

public class App {

    @Test
    public void test (){
        String r1 = recette1();
        String r2 = recette2();
        System.out.println(r1);
        System.out.println(r2);
        assertEquals(r1, r2);
    }

    private String recette1() {
        model.Customers client = new model.Customers("Nestor");

        //Creation des films
        model.Movie BiB = new model.Movie("Ben Is Back", model.Movie.NEW_RELEASE);
        model.Movie C2 = new model.Movie("Creed II", model.Movie.NEW_RELEASE);
        model.Movie TS = new model.Movie("Toys Story", model.Movie.CHILDRENS);
        model.Movie HP1 = new model.Movie("Harry Potter I", model.Movie.REGULAR);

        //Creation des locations
        model.Rental r1 = new model.Rental(BiB, 2);
        model.Rental r2 = new model.Rental(C2, 5);
        model.Rental r3 = new model.Rental(TS, 2);
        model.Rental r4 = new model.Rental(HP1, 6);

        //Attribution des locations au client "Nestor"
        client.addRental(r1);
        client.addRental(r2);
        client.addRental(r3);
        client.addRental(r4);

        return client.statement();
    }

    private String recette2() {
        legacy.Customers client = new legacy.Customers("Nestor");

        //Creation des films
        legacy.Movie BiB = new legacy.Movie("Ben Is Back", legacy.Movie.NEW_RELEASE);
        legacy.Movie C2 = new legacy.Movie("Creed II", legacy.Movie.NEW_RELEASE);
        legacy.Movie TS = new legacy.Movie("Toys Story", legacy.Movie.CHILDRENS);
        legacy.Movie HP1 = new legacy.Movie("Harry Potter I", legacy.Movie.REGULAR);

        //Creation des locations
        legacy.Rental r1 = new legacy.Rental(BiB, 2);
        legacy.Rental r2 = new legacy.Rental(C2, 5);
        legacy.Rental r3 = new legacy.Rental(TS, 2);
        legacy.Rental r4 = new legacy.Rental(HP1, 6);

        //Attribution des locations au client "Nestor"
        client.addRental(r1);
        client.addRental(r2);
        client.addRental(r3);
        client.addRental(r4);

        return client.statement();
    }
}
