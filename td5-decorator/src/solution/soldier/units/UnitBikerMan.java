/**
 * D. Auber & P. Narbel
 * Solution TD Architecture Logicielle 2016 Universit� Bordeaux.
 */
package solution.soldier.units;

import solution.soldier.core.EquipmentException;
import solution.soldier.core.UnitRider;
import solution.soldier.core.Equipment;

public class UnitBikerMan extends UnitRider {

	public UnitBikerMan(String soldierName) {
		super(soldierName, new BehaviorSoldierStd(20, 120));
	}

	/**
	 * A BikerMan can have at most one weapon
	 */
	@Override
	public void addEquipment(Equipment w) {
		if (nbWeapons() > 0)
			throw new EquipmentException();
		super.addEquipment(w);
	}

}
