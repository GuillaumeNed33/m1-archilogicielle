package work.soldiers;


public abstract class Soldier implements Unit {
    private String name;
    private Capacity capacity;

    Soldier(String name, Capacity cap) {
        this.name = name;
        this.capacity = cap;
    }

    @Override
    public boolean isAlive() {
        return capacity.getHealthPoints() > 0;
    }

    @Override
    public double strike() {
        return capacity.getForce();
    }

    @Override
    public void parry(double strike) {
        capacity.parry(strike);
    }
}
