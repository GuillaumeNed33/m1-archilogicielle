package work.soldiers;

public class Horseman extends Soldier {
    public Horseman(String name) {
        super(name, new Capacity(150, 40));
    }
}
