package work.soldiers;

public interface Unit {
    boolean isAlive();
    double strike();
    void parry(double strike);
}
