package work.soldiers;

public class Infantryman extends Soldier {
    public Infantryman(String name) {
        super(name, new Capacity(100, 50));
    }
}
