package work.soldiers;

public class Capacity {
    private float healthPoints;
    private float force;

    public Capacity(float health, float force) {
        this.healthPoints = health;
        this.force = force;
    }

    public float getHealthPoints() {
        return healthPoints;
    }

    public float getForce() {
        return force;
    }

    public void parry(double strike) {
        healthPoints -= (strike - force/2);
    }
}
