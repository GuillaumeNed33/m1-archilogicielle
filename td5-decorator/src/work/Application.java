package work;

import org.junit.Test;
import work.decoratedSoldiers.SoldierWithShield;
import work.decoratedSoldiers.SoldierWithSword;
import work.soldiers.*;

public class Application {

    @Test
    public void fight() {
        Unit s1 = new Infantryman("Guerrier");
        Unit s2 = new Horseman("Chevalier");
        Unit attack = new SoldierWithSword(s1);
        Unit defend = new SoldierWithShield(s2);

        while (s1.isAlive() && s2.isAlive()) {
            defend.parry(attack.strike());
            Unit tmpSwap = attack;
            attack = defend;
            defend = tmpSwap;
        }
        if (s1.isAlive())
            System.out.println("SoldierDecorator 1  Won");
        else
            System.out.println("SoldierDecorator 2  Won");
    }
}
