package work.decoratedSoldiers;

import work.soldiers.Unit;

public class SoldierWithSword extends SoldierDecorator {

    public SoldierWithSword(Unit s) {
        super(s);
    }

    @Override
    public boolean isAlive() {
        return super.isAlive();
    }

    @Override
    public double strike() {
        return super.strike()*2;
    }

    @Override
    public void parry(double strike) {
        super.parry(strike*1.2);
    }
}

