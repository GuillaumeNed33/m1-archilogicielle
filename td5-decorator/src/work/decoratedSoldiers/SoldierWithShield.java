package work.decoratedSoldiers;

import work.soldiers.Unit;

public class SoldierWithShield extends SoldierDecorator {

    public SoldierWithShield(Unit s) {
        super(s);
    }

    @Override
    public boolean isAlive() {
        return super.isAlive();
    }

    @Override
    public double strike() {
        return super.strike()*2;
    }

    @Override
    public void parry(double strike) {
        super.parry(strike*1.2);
    }
}

