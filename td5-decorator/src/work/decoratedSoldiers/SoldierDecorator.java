package work.decoratedSoldiers;

import work.soldiers.Unit;

public abstract class SoldierDecorator implements Unit {
    private Unit decoratedSoldier;

    public SoldierDecorator(Unit s) {
        decoratedSoldier = s;
    }

    @Override
    public boolean isAlive() {
        return decoratedSoldier.isAlive();
    }

    @Override
    public double strike() {
        return decoratedSoldier.strike();
    }

    @Override
    public void parry(double strike) {
        decoratedSoldier.parry(strike);
    }
}
