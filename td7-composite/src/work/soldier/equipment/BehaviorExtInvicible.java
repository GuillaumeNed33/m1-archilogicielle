/**
 * D. Auber & P. Narbel
 * Solution TD Architecture Logicielle 2016 Universit� Bordeaux.
 */
package work.soldier.equipment;

import work.soldier.core.BehaviorExtension;
import work.soldier.core.BehaviorSoldier;
import work.soldier.core.Equipment;

public class BehaviorExtInvicible extends BehaviorExtension {
	public BehaviorExtInvicible(Equipment owner, BehaviorSoldier s) {
		super(owner, s);
	}

	@Override
	public float parry(float force) {
		return super.parry(0);
	}
}
