package work.soldier.core;

import java.util.Iterator;

/**
 * Unit est un composant (pattern composite)
 */
public interface Unit {
    public String getName();
    public float getHealthPoints();
    public boolean alive();
    public void heal();
    public float parry(float force);
    public float strike();

    public void addEquipment(Equipment w);
    public void removeEquipment(Equipment w);
    public Iterator<Equipment> getEquipments();

    public Iterator<Unit> getUnits();
    public void addUnit(Unit au);
    public void removeUnit(Unit au);
}
