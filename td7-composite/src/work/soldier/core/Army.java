package work.soldier.core;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Pattern composite ; Une armée est composé d'Units
 */
public class Army implements Unit {
    private List<Unit> units;
    private String name;

    public Army(String name) {
        this.name = name;
        this.units = new ArrayList<Unit>();
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public float getHealthPoints() {
        float res = 0;
        for (Unit u : units)
            res += u.getHealthPoints();
        return res;
    }

    @Override
    public boolean alive() {
        return getHealthPoints() > 0.f;
    }

    @Override
    public void heal() {
        for (Unit u : units)
            u.heal();
    }

    @Override
    public float parry(float force) {
        while(alive() && force > 0) {
            float nbUnits = 0.f;
            for(Unit u : units)
                if (u.alive()) ++nbUnits;

            float toParry = force / nbUnits;
            force = 0.f;
            for(Unit u : units) {
                if (!u.alive()) continue;
                force += u.parry(toParry);
            }
        }
        return force;
    }

    @Override
    public float strike() {
        float res = 0;
        for (Unit u : units)
            if(u.alive())
                res += u.strike();
        return res;
    }

    @Override
    public void addEquipment(Equipment w) {
        Iterator<Unit> it = getUnits();
        while (it.hasNext()) {
            Unit u = it.next();
            try {
                u.addEquipment(w);
                return;
            } catch(EquipmentException b) {
                if (!it.hasNext()) throw b;
            }
        }
    }

    @Override
    public void removeEquipment(Equipment w) {
        for (Iterator<Unit> it = getUnits(); it.hasNext(); it.next().removeEquipment(w)) {}
    }

    @Override
    public Iterator<Equipment> getEquipments() {
        return null;
    }

    @Override
    public Iterator<Unit> getUnits() {
        return units.iterator();
    }

    @Override
    public void addUnit(Unit au) {
        units.add(au);
    }

    @Override
    public void removeUnit(Unit au) {
        units.remove(au);
    }
}
