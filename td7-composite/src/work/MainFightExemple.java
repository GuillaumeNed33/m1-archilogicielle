package work;
/**
 * D. Auber & P. Narbel
 * Solution TD Architecture Logicielle 2016 Universit� Bordeaux.
 */


import work.soldier.core.Army;
import work.soldier.core.Equipment;
import work.soldier.core.Unit;
import work.soldier.equipment.WeaponShield;
import work.soldier.units.UnitCenturion;
import work.soldier.units.UnitHorseMan;

public class MainFightExemple {
	
	public static void main(String[] args) {
		Unit hm = new UnitHorseMan("Chevalier1");
		Unit im = new UnitCenturion("Centurion1");
		//Unit gr1 = new UnitHorseMan("HorsemanGR1");
		Unit gr1 = new Army("Armée 1");
		gr1.addUnit(hm);
		gr1.addUnit(im);
		Unit hm2 = new UnitCenturion("Centurion2");
		Unit im2 = new UnitHorseMan("Chevalier2");
		Unit gr2 = new Army("Armée 2");
		gr2.addUnit(hm2);
		gr2.addUnit(im2);
		Unit gr3 = new Army("Armée 3");
		gr3.addUnit(gr1);
		gr3.addUnit(gr2);
		Equipment shield = new WeaponShield();
		gr3.addEquipment(shield);
		System.out.println("la force de frappe de " + hm.getName() + " est de : " + hm.strike());
		System.out.println("la force de frappe de " + im.getName() + " est de : " + im.strike());
		System.out.println("la force de frappe de l'armée " + gr1.getName() + " est de : " + gr1.strike());

		System.out.println("");

		System.out.println("la force de frappe de " + im2.getName() + " est de : " + im2.strike());
		System.out.println("la force de frappe de " + hm2.getName() + " est de : " + hm2.strike());
		System.out.println("la force de frappe de l'armée " + gr2.getName() + " est de : " + gr2.strike());

		System.out.println("");

		System.out.println("la force de frappe de l'armée " + gr3.getName() + " est de : " + gr3.strike());
	}
}
