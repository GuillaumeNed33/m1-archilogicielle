/**
 * D. Auber & P. Narbel
 * Solution TD Architecture Logicielle 2016 Universit� Bordeaux.
 */
package solution.soldier.ages;

import solution.soldier.core.AgeAbstractFactory;
import solution.soldier.core.Unit;
import solution.soldier.equipment.EquipmentTrumpet;
import solution.soldier.equipment.WeaponShield;
import solution.soldier.equipment.WeaponSword;
import solution.soldier.core.Equipment;
import solution.soldier.units.UnitCenturion;
import solution.soldier.units.UnitHorseMan;

public class AgeMiddleFactory implements AgeAbstractFactory {

	@Override
	public Unit infantryUnit(String name) {
		return new UnitCenturion(name);
	}

	@Override
	public Unit riderUnit(String name) {
		return new UnitHorseMan(name);
	}

	@Override
	public Equipment attackWeapon() {
		return new WeaponSword();
	}

	@Override
	public Equipment defenseWeapon() {
		return new WeaponShield();
	}
	
	@Override
	public Equipment toy() {
		return new EquipmentTrumpet();
	}
}
