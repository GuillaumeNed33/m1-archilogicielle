/**
 * D. Auber & P. Narbel
 * Solution TD Architecture Logicielle 2016 Universit� Bordeaux.
 */
package solution.soldier.ages;

import solution.soldier.core.AgeAbstractFactory;
import solution.soldier.core.Unit;
import solution.soldier.equipment.EquipmentTrumpet;
import solution.soldier.equipment.WeaponGun;
import solution.soldier.equipment.WeaponShield;
import solution.soldier.core.Equipment;
import solution.soldier.units.UnitBikerMan;
import solution.soldier.units.UnitRobot;

public class AgeFutureFactory implements AgeAbstractFactory {

	@Override
	public Unit infantryUnit(String name) {
		return new UnitRobot(name);
	}

	@Override
	public Unit riderUnit(String name) {
		return new UnitBikerMan(name);
	}

	@Override
	public Equipment attackWeapon() {
		return new WeaponGun();
	}

	@Override
	public Equipment defenseWeapon() {
		return new WeaponShield();
	}
	
	@Override
	public Equipment toy() {
		return new EquipmentTrumpet();
	}

}
