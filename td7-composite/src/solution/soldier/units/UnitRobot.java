/**
 * D. Auber & P. Narbel
 * Solution TD Architecture Logicielle 2016 Universit� Bordeaux.
 */
package solution.soldier.units;

import solution.soldier.core.EquipmentException;
import solution.soldier.core.UnitInfantry;
import solution.soldier.core.Equipment;

public class UnitRobot extends UnitInfantry {

	public UnitRobot(String soldierName) {
		super(soldierName, new BehaviorSoldierHealthBased( 50, 100));
	}
	
	/**
	 * A Robot can have at most four equipments
	 */
	@Override
	public void addEquipment(Equipment w) {
		if (nbWeapons()>3) throw new EquipmentException();
		super.addEquipment(w);
	}

	
}
