/**
 * D. Auber & P. Narbel
 * Solution TD Architecture Logicielle 2016 Universit� Bordeaux.
 */
package solution.soldier.equipment;

import solution.soldier.core.EquipmentAttack;
import solution.soldier.core.EquipmentDefense;
import solution.soldier.core.EquipmentToy;

public interface WeaponVisitor {
	void visit(EquipmentAttack s);
	void visit(EquipmentDefense s);
	void visit(EquipmentToy s);
}
