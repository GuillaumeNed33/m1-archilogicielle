/**
 * D. Auber & P. Narbel
 * Solution TD Architecture Logicielle 2016 Universit� Bordeaux.
 */
package solution.soldier.equipment;

import solution.soldier.core.BehaviorExtension;
import solution.soldier.core.BehaviorSoldier;
import solution.soldier.core.Equipment;

public class BehaviorExtInvicible extends BehaviorExtension {
	public BehaviorExtInvicible(Equipment owner, BehaviorSoldier s) {
		super(owner, s);
	}

	@Override
	public float parry(float force) {
		return super.parry(0);
	}
}
