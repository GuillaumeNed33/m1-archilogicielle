package work.model;

public interface UIBuilder {
    void initialize();
    void finalizeUI();

    void beginHeader();
    void endHeader();

    void beginTable();
    void endTable();

    void beginRow();
    void endRow();

    void beginColumn();
    void endColumn();

    void beginParagraph();
    void endParagraph();

    void addString(String s);
}
