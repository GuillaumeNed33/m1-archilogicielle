package work.model;

public interface StatementBuilder {
    void createNewStatement();
    void buildHeader(String name);
    void buildRental(Rental rental);
    void buildAmount(double amount);
    void buildRenterPoints(int renterPoints);
    void finalizeStatement();
    Object getStatement();
}
