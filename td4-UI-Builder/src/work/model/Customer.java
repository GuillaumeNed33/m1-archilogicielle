package work.model;

import java.util.*;

public class Customer
{
	private String _name;
	private List<Rental> _rentals = new ArrayList<Rental>();

	public Customer(String name)
	{
		_name=name;
	}

	public void addRental(Rental rental)
	{
		_rentals.add(rental);
	}

	public String getName()
	{
		return _name;
	}

	@Deprecated
	public String statement(String endl, String tab)
	{
		String result = "Rental Record for "+getName()+ endl;
		for(Rental each : _rentals)
		{
			result +=
					tab + each.getMovie().getTitle()
							+ tab + String.valueOf(each.getAmount())
							+ " " + endl;
		}

		result += "Amount owed is " + String.valueOf(getTotalAmount()) +
				endl;
		result += "You earned " + String.valueOf(getRenterPoints()) +
				" frequent renter points";
		return result;
	}

	@Deprecated
	public String statementHTML()
	{
		StringBuilder result = new StringBuilder();
		result.append("<html><body>\n");
		result.append(statement("<br>", "&nbsp;&nbsp;"));
		result.append("</body></html>");
		return result.toString();
	}

	@Deprecated
	public String statement()
	{
		return statement("\n","\t");
	}


	public void buildStatement(StatementBuilder bld) {
		bld.createNewStatement();
		bld.buildHeader(getName());

		for(Rental each : getRentals())
		{
			bld.buildRental(each);
		}

		bld.buildAmount(getTotalAmount());
		bld.buildRenterPoints(getRenterPoints());

		bld.finalizeStatement();
	}

	public void buildUI(UIBuilder bld) {
		bld.initialize();

		bld.beginHeader();
		bld.addString("Rental Record for " + getName());
		bld.endHeader();

		bld.beginTable();
		for(Rental each : getRentals())
		{
			Movie mov = each.getMovie();
			bld.beginRow();
			bld.beginColumn();
			bld.addString(mov.getTitle());
			bld.endColumn();
			bld.beginColumn();
			bld.addString(String.valueOf(each.getDaysRented()) );
			bld.endColumn();
			bld.beginColumn();
			bld.addString(String.valueOf(each.getAmount()) );
			bld.endColumn();
			bld.endRow();
		}
		bld.endTable();

		bld.beginParagraph();
		bld.addString("Amount owed is : " + String.valueOf(getTotalAmount()));
		bld.endParagraph();

		bld.beginParagraph();
		bld.addString("You earned : " + String.valueOf(getRenterPoints()) + " frequent renter points.");
		bld.endParagraph();

		bld.finalizeUI();
	}

	private ArrayList<Rental> getRentals() {
		return new ArrayList<>(_rentals);
	}


	private double getTotalAmount()
	{
		double res = 0;
		for(Rental each : _rentals)
		{
			res += each.getAmount();
		}
		return res;
	}

	private int getRenterPoints()
	{
		int res = 0;
		for(Rental each : _rentals)
		{
			res += each.getRenterPoints();
		}
		return res;
	}
}
