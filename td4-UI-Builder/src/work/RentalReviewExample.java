package work;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import work.model.Customer;

import work.model.*;

import java.util.ArrayList;
import java.util.List;

public class RentalReviewExample extends Application {

    @Override
    public void start(Stage primaryStage) {

        /** DATA INITIALIZATION **/
        Customer client = new Customer("Luc Abalo");
        //Creation des films
        Movie BiB = new Movie("Ben Is Back", Movie.NEW_RELEASE);
        Movie C2 = new Movie("Creed II", Movie.NEW_RELEASE);
        Movie TS = new Movie("Toys Story", Movie.CHILDRENS);
        Movie HP1 = new Movie("Harry Potter I", Movie.REGULAR);

        //Attribution des locations au client "Nestor"
        client.addRental(new Rental(BiB, 2));
        client.addRental(new Rental(C2, 5));
        client.addRental(new Rental(TS, 2));
        client.addRental(new Rental(HP1, 6));

        List<Rental> rentals = new ArrayList<>();
        rentals.add(new Rental(BiB, 2));
        rentals.add(new Rental(C2, 5));
        rentals.add(new Rental(TS, 2));
        rentals.add(new Rental(HP1, 6));

        /** UI BUILDING **/
        //Title
        Text title = new Text("Rental Review for : " + client.getName().toString());
        title.setStyle("-fx-font: 28 arial;");

        // Build a first grid
        GridPane gridRentals = new GridPane();
        gridRentals.setGridLinesVisible(true); // Useful for debug

        // Adjust the grid style
        gridRentals.setHgap(10);
        gridRentals.setVgap(10);
        gridRentals.setPadding(new Insets(10, 10, 10, 10));

        // Create header grid
        Text headerTitle = new Text("Movie title");
        headerTitle.setStyle("-fx-font: 24 arial;");
        gridRentals.add(headerTitle, 0, 0);

        Text headerDaysRented = new Text("Rented days");
        headerDaysRented.setStyle("-fx-font: 24 arial;");
        gridRentals.add(headerDaysRented, 1, 0);

        Text headerPrice = new Text("Price");
        headerPrice.setStyle("-fx-font: 24 arial;");
        gridRentals.add(headerPrice, 2, 0);

        /** MOVIE IN GRID **/
        int i = 1;
        for(Rental r : rentals) {
            Text movie = new Text(r.getMovie().getTitle());
            movie.setStyle("-fx-font: 20 arial;");

            Text days = new Text(Integer.toString(r.getDaysRented()));
            days.setStyle("-fx-font: 20 arial;");

            Text price = new Text(Double.toString(r.getAmount()));
            price.setStyle("-fx-font: 20 arial;");

            gridRentals.add(movie, 0, i);
            gridRentals.add(days, 1, i);
            gridRentals.add(price, 2, i);
            i++;
        }

        // Put the two grids in a column
        VBox col = new VBox();
        col.setAlignment(Pos.BASELINE_CENTER);
        col.getChildren().add(title);
        col.getChildren().add(gridRentals);

        // JavaFX uses Scenes for content
        Scene scene = new Scene(col, 600, 800);

        // Scenes are displayed by a Stage
        primaryStage.setTitle("Rental Review");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}