package work.ui;

import work.model.Rental;
import work.model.StatementBuilder;

public class StringStatement implements StatementBuilder {

    private String statement;

    @Override
    public void createNewStatement() {
        statement = "";
    }

    @Override
    public void buildHeader(String name) {
        statement += "Rental Record for " + name + "\n";
    }

    @Override
    public void buildRental(Rental rental) {
        statement += "\t" + rental.getMovie().getTitle()
                 + "( " + String.valueOf(rental.getDaysRented()) + " days of rental ) : "
                 + String.valueOf(rental.getAmount())
                 + "\n";
    }

    @Override
    public void buildAmount(double amount) {
        statement += "Amount owed is " + String.valueOf(amount) + "\n";
    }

    @Override
    public void buildRenterPoints(int renterPoints) {
        statement += "You earned " + String.valueOf(renterPoints) + " frequent renter points.\n";
    }

    @Override
    public void finalizeStatement() {
        statement += "</body></html>";
    }

    @Override
    public Object getStatement() {
        return statement;
    }
}
