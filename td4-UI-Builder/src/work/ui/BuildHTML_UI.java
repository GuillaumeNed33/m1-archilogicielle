package work.ui;

import work.model.UIBuilder;

public class BuildHTML_UI implements UIBuilder {
    String _string;

    public BuildHTML_UI()
    {
    }

    @Override
    public void beginHeader()
    {
        _string += "<h1>";
    }
    @Override
    public void endHeader()
    {
        _string += "</h1>";
    }

    @Override
    public void beginTable()
    {
        _string += "<table>";
    }

    @Override
    public void endTable()
    {
        _string += "</table>";
    }

    @Override
    public void beginRow()
    {
        _string += "<tr>";
    }

    @Override
    public void endRow()
    {
        _string += "</tr>";
    }

    @Override
    public void beginColumn()
    {
        _string += "<td>";
    }

    @Override
    public void endColumn()
    {
        _string += "</td>";
    }

    @Override
    public void beginParagraph()
    {
        _string += "<p>";
    }

    @Override
    public void endParagraph()
    {
        _string += "</p>";
    }

    @Override
    public void addString(String s)
    {
        _string += s;
    }

    @Override
    public void finalizeUI()
    {
        _string += "</body></html>";
    }

    @Override
    public void initialize()
    {
        _string = "<!doctype html>"
                + "<html>"
                + "<head>"
                + "<title>Statement</title>"
                + "</head>"
                + "<body>";
    }

    public String getUI()
    {
        return _string;
    }
}
