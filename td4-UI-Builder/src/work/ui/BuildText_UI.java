package work.ui;

import work.model.UIBuilder;

public class BuildText_UI implements UIBuilder {
    StringBuilder _string = new StringBuilder();

    public BuildText_UI()
    {
    }

    @Override
    public void beginHeader()
    {

    }
    @Override
    public void endHeader()
    {
        _string.append("\n");
    }

    @Override
    public void beginTable()
    {

    }

    @Override
    public void endTable()
    {

    }

    @Override
    public void beginRow()
    {
    }

    @Override
    public void endRow()
    {
        _string.append("\n");
    }

    @Override
    public void beginColumn()
    {
        _string.append("\t");
    }

    @Override
    public void endColumn()
    {
    }

    @Override
    public void beginParagraph()
    {
        _string.append("\n");
    }

    @Override
    public void endParagraph()
    {

    }

    @Override
    public void addString(String s)
    {
        _string.append(s);
    }

    @Override
    public void finalizeUI()
    {

    }

    @Override
    public void initialize()
    {
        _string = new StringBuilder();
    }

    public String getUI()
    {
        return _string.toString();
    }
}
