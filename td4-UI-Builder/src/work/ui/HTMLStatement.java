package work.ui;

import work.model.Rental;
import work.model.StatementBuilder;

import java.io.*;

public class HTMLStatement implements StatementBuilder {

    private String statement;

    @Override
    public void createNewStatement() {
        statement = "<!doctype html>" +
                "<html lang=\"fr\">" +
                "<head>" +
                "  <meta charset=\"utf-8\">" +
                "  <title>HTML Statement</title>" +
                "</head>" +
                "<body>";
    }

    @Override
    public void buildHeader(String name) {
        statement += "<h1>Rental Record for " + name + "</h1>"
                + "<table>"
                + "<tr><td>Movie Title</td>"
                + "<td>Days of rental</td>"
                + "<td>Price</td></tr>";
    }

    @Override
    public void buildRental(Rental rental) {
        statement += "<tr><td>" + rental.getMovie().getTitle() + "</td>"
                + "<td>" + rental.getDaysRented() + "</td>"
                + "<td>" + rental.getAmount() + "</td></tr>";
    }

    @Override
    public void buildAmount(double amount) {
        statement += "</table>" +
                "<p>Amount owed is " + String.valueOf(amount) + "</p>";
    }

    @Override
    public void buildRenterPoints(int renterPoints) {
        statement += "<p>You earned " + String.valueOf(renterPoints) + " frequent renter points.</p>";
    }

    @Override
    public void finalizeStatement() {
        statement += "</body></html>";
    }

    @Override
    public Object getStatement() {
        try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream("statement.html"), "utf-8"))) {
            writer.write(statement);
            writer.close();
        } catch (IOException ex) {
            throw new Error(ex);
        }
        return statement;
    }
}
