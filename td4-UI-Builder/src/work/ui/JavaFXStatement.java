package work.ui;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import work.model.Rental;
import work.model.StatementBuilder;

public class JavaFXStatement implements StatementBuilder {
    private VBox col;
    private GridPane gridRentals;
    private int i;


    @Override
    public void createNewStatement() {
        i = 1;
        col = new VBox();
        gridRentals = new GridPane();
        col.setAlignment(Pos.BASELINE_CENTER);
        gridRentals.setGridLinesVisible(true); // Useful for debug
        gridRentals.setHgap(10);
        gridRentals.setVgap(10);
        gridRentals.setPadding(new Insets(10, 10, 10, 10));

        // Create header grid
        Text headerTitle = new Text("Movie title");
        headerTitle.setStyle("-fx-font: 24 arial;");
        gridRentals.add(headerTitle, 0, 0);

        Text headerDaysRented = new Text("Rented days");
        headerDaysRented.setStyle("-fx-font: 24 arial;");
        gridRentals.add(headerDaysRented, 1, 0);

        Text headerPrice = new Text("Price");
        headerPrice.setStyle("-fx-font: 24 arial;");
        gridRentals.add(headerPrice, 2, 0);
    }

    @Override
    public void buildHeader(String name) {
        Text title = new Text("Rental Review for : " + name);
        title.setStyle("-fx-font: 28 arial;");
        col.getChildren().add(title);
    }

    @Override
    public void buildRental(Rental rental) {
        Text movie = new Text(rental.getMovie().getTitle());
        movie.setStyle("-fx-font: 20 arial;");

        Text days = new Text(Integer.toString(rental.getDaysRented()));
        days.setStyle("-fx-font: 20 arial;");

        Text price = new Text(Double.toString(rental.getAmount()));
        price.setStyle("-fx-font: 20 arial;");

        gridRentals.add(movie, 0, i);
        gridRentals.add(days, 1, i);
        gridRentals.add(price, 2, i);
        this.i++;
    }

    @Override
    public void buildAmount(double amount) {
        Text amountText = new Text("Amount owed is " + String.valueOf(amount));
        amountText.setStyle("-fx-font: 28 arial;");
        col.getChildren().add(amountText);
    }

    @Override
    public void buildRenterPoints(int renterPoints) {
        Text points = new Text("You earned " + String.valueOf(renterPoints) + " frequent renter points.");
        points.setStyle("-fx-font: 28 arial;");
        col.getChildren().add(points);
    }

    @Override
    public void finalizeStatement() {
        col.getChildren().add(gridRentals);
    }

    @Override
    public Object getStatement() {
        return col;
    }
}
