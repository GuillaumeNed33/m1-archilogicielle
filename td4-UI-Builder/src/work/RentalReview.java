package work;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import work.model.*;
import work.ui.*;

public class RentalReview extends Application {

    @Override
    public void start(Stage primaryStage) {

        /** DATA INITIALIZATION **/
        Customer client = new Customer("Luc Abalo");
        //Creation des films
        Movie BiB = new Movie("Ben Is Back", Movie.NEW_RELEASE);
        Movie C2 = new Movie("Creed II", Movie.NEW_RELEASE);
        Movie TS = new Movie("Toys Story", Movie.CHILDRENS);
        Movie HP1 = new Movie("Harry Potter III", Movie.REGULAR);

        //Attribution des locations au client "Nestor"
        client.addRental(new Rental(BiB, 2));
        client.addRental(new Rental(C2, 5));
        client.addRental(new Rental(TS, 2));
        client.addRental(new Rental(HP1, 6));

        StatementBuilder statementString = new StringStatement();
        client.buildStatement(statementString);
        System.out.println((String)statementString.getStatement());

        StatementBuilder statementHTML = new HTMLStatement();
        client.buildStatement(statementHTML);
        System.out.println((String)statementHTML.getStatement());

        StatementBuilder statementJFX = new JavaFXStatement();
        client.buildStatement(statementJFX);
        VBox col = ((VBox)statementJFX.getStatement());

        /*** UI Builder ***/
        UIBuilder UIString = new BuildText_UI();
        client.buildUI(UIString);
        System.out.println(((BuildText_UI) UIString).getUI());

        UIBuilder UIHTML = new BuildHTML_UI();
        client.buildUI(UIHTML);
        System.out.println(((BuildHTML_UI) UIHTML).getUI());

        UIBuilder UIJFX = new BuildJavaFX_UI();
        client.buildUI(UIJFX);
        GridPane gridPane = (((BuildJavaFX_UI) UIJFX).getUI());


        // JavaFX uses Scenes for content
        Scene scene = new Scene(col, 600, 800);
        Scene scene2 = new Scene(gridPane, 600, 800);

        // Scenes are displayed by a Stage
        primaryStage.setTitle("Rental Review");
        primaryStage.setScene(scene2);
        primaryStage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }
}