/**
 * D. Auber & P. Narbel
 * Solution TD Architecture Logicielle 2016 Universit� Bordeaux.
 */
package solution.soldier.core;

import solution.soldier.equipment.WeaponVisitor;

public abstract class EquipmentToy extends EquipmentAbstract {

	@Override
	public void accept(WeaponVisitor v) {
		v.visit(this);
	}
}
