package work.soldier.core;

public interface UnitObserver {
    void update(Unit unit);
}
