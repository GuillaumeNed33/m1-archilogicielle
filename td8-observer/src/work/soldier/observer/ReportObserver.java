package work.soldier.observer;

import work.soldier.core.Unit;
import work.soldier.core.UnitObserver;

public class ReportObserver implements UnitObserver {
    public void update(Unit unit) {
        System.out.println("HIT : L'unité : " + unit.getName() + " a été touchée.");
    }
}
