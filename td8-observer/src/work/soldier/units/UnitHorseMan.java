/**
 * D. Auber & P. Narbel
 * Solution TD Architecture Logicielle 2016 Universit� Bordeaux.
 */
package work.soldier.units;

import work.soldier.core.Equipment;
import work.soldier.core.EquipmentException;
import work.soldier.core.UnitRider;

public class UnitHorseMan extends UnitRider {

	public UnitHorseMan(String soldierName) {
		super(soldierName, new BehaviorSoldierStd(2000, 120));
	}

	/**
	 * A HorseMan can only have two equipments, and one of each kind
	 */
	@Override
	public void addEquipment(Equipment w) {
		int nbW = nbWeapons();
		if (nbW > 1)
			throw new EquipmentException();
		if (nbW == 1 && getEquipments().next().getClass() == w.getClass())
			throw new EquipmentException();
		super.addEquipment(w);
	}

}
