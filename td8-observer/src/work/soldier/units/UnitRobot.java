/**
 * D. Auber & P. Narbel
 * Solution TD Architecture Logicielle 2016 Universit� Bordeaux.
 */
package work.soldier.units;

import work.soldier.core.EquipmentException;
import work.soldier.core.UnitInfantry;
import work.soldier.core.Equipment;

public class UnitRobot extends UnitInfantry {

	public UnitRobot(String soldierName) {
		super(soldierName, new BehaviorSoldierStd( 5000, 100));
	}
	
	/**
	 * A Robot can have at most four equipments
	 */
	@Override
	public void addEquipment(Equipment w) {
		if (nbWeapons()>3) throw new EquipmentException();
		super.addEquipment(w);
	}

	
}
