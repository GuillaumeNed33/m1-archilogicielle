/**
 * D. Auber & P. Narbel
 * Solution TD Architecture Logicielle 2016 Universit� Bordeaux.
 */
package work.test;


import work.soldier.ages.AgeFutureFactory;
import work.soldier.ages.AgeMiddleFactory;
import work.soldier.core.*;
import work.soldier.equipment.WeaponShield;
import work.soldier.observer.KillObserver;
import work.soldier.observer.ReportObserver;
import work.soldier.units.UnitCenturion;
import work.soldier.units.UnitHorseMan;

public class MainFightTwoAges {

	public static UnitObserver obs;
	public static UnitObserver obsKill;

	public static Unit createTeam(AgeAbstractFactory fact, String prefix)  {
		String nameSG = "";
		String nameWorms = "";
		String namePig = "";
		String nameNicky = "";
		String nameTomy = "";
		String nameJenny = "";
		String nameKenny = "";
		if(fact instanceof  AgeMiddleFactory) {
			nameSG = "Templiers";
			nameWorms = "Guerriers";
			namePig = "Chevaliers";
			nameNicky = "Arthur";
			nameTomy = "Merlin";
			nameJenny = "Lancelot";
			nameKenny = "Gabriel";
		}
		else {
			nameSG = "US_Army";
			nameWorms = "Soldats";
			namePig = "Pilotes";
			nameNicky = "Scott";
			nameTomy = "Tom";
			nameJenny = "Mike";
			nameKenny = "Tony";
		}

		UnitGroup sg = new UnitGroup(prefix + nameSG);
		UnitGroup worms = new UnitGroup(prefix + nameWorms);
		UnitGroup pig = new UnitGroup(prefix + namePig);

/*		pig.addObserver(obs);
		worms.addObserver(obs);
		sg.addObserver(obs);
		pig.addObserver(obsKill);
		worms.addObserver(obsKill);
		sg.addObserver(obsKill);*/

		sg.addUnit(pig);
		sg.addUnit(worms);

		Unit nicky = fact.infantryUnit(prefix + nameNicky);
		Unit tomy = fact.infantryUnit(prefix + nameTomy);
		tomy.addObserver(obs);
		nicky.addObserver(obs);
		tomy.addObserver(obsKill);
		nicky.addObserver(obsKill);

		worms.addUnit(nicky);
		worms.addUnit(tomy);
		worms.addEquipment(fact.attackWeapon());
		worms.addEquipment(fact.defenseWeapon());
		worms.addEquipment(fact.attackWeapon());
		worms.addEquipment(fact.toy());

		Unit jenny = fact.riderUnit(prefix + nameJenny);
		Unit kenny = fact.riderUnit(prefix + nameKenny);
		jenny.addObserver(obs);
		kenny.addObserver(obs);
		jenny.addObserver(obsKill);
		kenny.addObserver(obsKill);

		pig.addUnit(jenny);
		pig.addUnit(kenny);
		pig.addEquipment(fact.defenseWeapon());
		pig.addEquipment(fact.defenseWeapon());
		
		return sg;
	}

	public static void fightExample() {
		AgeAbstractFactory age1 = new AgeMiddleFactory();
		AgeAbstractFactory age2 = new AgeFutureFactory();

		Unit team1 = createTeam(age2, "Team1::");
		Unit team2 = createTeam(age1, "Team2::");

		int round = 0;
		while(team1.alive() && team2.alive()) {
			System.out.println("Round  #" + round++);
			float st1 = team1.strike();
			System.out.println(team1.getName() + " attack with force : " + st1);
			team2.parry(st1);
			float st2 = team2.strike();
			System.out.println(team2.getName() + " attack with force : " + st2);
			team1.parry(st2);
		}
		System.out.println("\n===================================================================================");
		System.out.println("The end ... " + (team1.alive() ? team1.getName() : team2.getName()) + " won." );
	}

	public static void main(String[] args) {
		obs = new ReportObserver();
		obsKill = new KillObserver();
		fightExample();
	}

}
