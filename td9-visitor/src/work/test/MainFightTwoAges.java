/**
 * D. Auber & P. Narbel
 * Solution TD Architecture Logicielle 2016 Universit� Bordeaux.
 */
package work.test;


import work.soldier.ages.AgeFutureFactory;
import work.soldier.ages.AgeMiddleFactory;
import work.soldier.core.*;
import work.soldier.observer.KillObserver;
import work.soldier.observer.ReportObserver;
import work.soldier.visitor.EquipmentRegisterVisitor;
import work.soldier.visitor.UnitCounterVisitor;
import work.soldier.visitor.UnitRiderVisitor;

public class MainFightTwoAges {

	public static UnitObserver obs;
	public static UnitObserver obsKill;
	public static final int HEALTH = 1750;

	public static Unit createTeam(AgeAbstractFactory fact, String prefix)  {
		String nameSG = "US_Army";
		String nameWorms = "Soldats";
		String namePig = "Pilotes";
		String nameNicky = "Scott";
		String nameTomy = "Tom";
		String nameJenny = "Mike";
		String nameKenny = "Tony";

		if(fact instanceof AgeMiddleFactory) {
			nameSG = "Templiers";
			nameWorms = "Guerriers";
			namePig = "Chevaliers";
			nameNicky = "Arthur";
			nameTomy = "Merlin";
			nameJenny = "Lancelot";
			nameKenny = "Gabriel";
		}

		UnitGroup sg = new UnitGroup(prefix + nameSG);
		UnitGroup worms = new UnitGroup(prefix + nameWorms);
		UnitGroup pig = new UnitGroup(prefix + namePig);

/*		pig.addObserver(obs);
		worms.addObserver(obs);
		sg.addObserver(obs);
		pig.addObserver(obsKill);
		worms.addObserver(obsKill);
		sg.addObserver(obsKill);*/

		sg.addUnit(pig);
		sg.addUnit(worms);

		Unit nicky = fact.infantryUnit(prefix + nameNicky);
		Unit tomy = fact.infantryUnit(prefix + nameTomy);
		tomy.addObserver(obs);
		nicky.addObserver(obs);
		tomy.addObserver(obsKill);
		nicky.addObserver(obsKill);

		worms.addUnit(nicky);
		worms.addUnit(tomy);
		worms.addEquipment(fact.attackWeapon());
		worms.addEquipment(fact.defenseWeapon());
		worms.addEquipment(fact.attackWeapon());
		worms.addEquipment(fact.toy());

		Unit jenny = fact.riderUnit(prefix + nameJenny);
		Unit kenny = fact.riderUnit(prefix + nameKenny);
		jenny.addObserver(obs);
		kenny.addObserver(obs);
		jenny.addObserver(obsKill);
		kenny.addObserver(obsKill);

		pig.addUnit(jenny);
		pig.addUnit(kenny);
		pig.addEquipment(fact.defenseWeapon());
		pig.addEquipment(fact.defenseWeapon());
		return sg;
	}
	private static void printVisitorStats(Unit team) {
		UnitCounterVisitor counterVisitor = new UnitCounterVisitor();
		EquipmentRegisterVisitor equimentVisitor = new EquipmentRegisterVisitor();
		UnitRiderVisitor ridersVisitor = new UnitRiderVisitor(HEALTH);

		team.accept(counterVisitor);
		team.accept(ridersVisitor);
		team.accept(equimentVisitor);

		System.out.println("\n***************** STATS VISITOR " + team.getName() +" *******************");
		System.out.println("Number of units in army " + team.getName() + " : " + counterVisitor.getNbSoldier());

		System.out.println("Riders with health >= " + HEALTH + " in army " + team.getName() + " : ");
		for(Unit u : ridersVisitor.getUnitRiderGoodHealth()) {
			System.out.println("\t- " + u.getName() + " (health = " + u.getHealthPoints() + ")");
		}

		System.out.println("Equipments list in army " + team.getName() + " : ");
		equimentVisitor.printAllEquiments();

		System.out.println("********************************************************************\n");
	}

	public static void fightExample() {
		AgeAbstractFactory age1 = new AgeMiddleFactory();
		AgeAbstractFactory age2 = new AgeFutureFactory();

		Unit team1 = createTeam(age2, "Team1::");
		Unit team2 = createTeam(age1, "Team2::");

		/** VISITOR PART **/
		final int HEALTH = 1750;

		printVisitorStats(team1);
		printVisitorStats(team2);

		System.out.println("=======================================================");
		System.out.println("=======================  FIGHT ! ======================");
		System.out.println("=======================================================\n");

		int round = 0;
		while(team1.alive() && team2.alive()) {
			System.out.println("Round  #" + round++);
			float st1 = team1.strike();
			System.out.println(team1.getName() + " attack with force : " + st1);
			team2.parry(st1);
			float st2 = team2.strike();
			System.out.println(team2.getName() + " attack with force : " + st2);
			team1.parry(st2);
			printVisitorStats(team1);
			printVisitorStats(team2);
		}
		System.out.println("\n===================================================================================");
		System.out.println("The end ... " + (team1.alive() ? team1.getName() : team2.getName()) + " won." );
	}



	public static void main(String[] args) {
		obs = new ReportObserver();
		obsKill = new KillObserver();
		fightExample();
	}

}
