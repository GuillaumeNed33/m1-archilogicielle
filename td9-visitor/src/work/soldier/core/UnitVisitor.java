package work.soldier.core;

public interface UnitVisitor {
    void visit(UnitRider u);
    void visit(UnitInfantry u);
    void visit(UnitGroup u);
}
