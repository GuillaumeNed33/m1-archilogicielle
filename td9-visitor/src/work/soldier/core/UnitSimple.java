/**
 * D. Auber & P. Narbel
 * Solution TD Architecture Logicielle 2016 Universit� Bordeaux.
 */
package work.soldier.core;

import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

public abstract class UnitSimple implements Unit {

	private BehaviorSoldier behavior;
	private String name;

	/**
	 * Observer attributes
	 */
	private List<UnitObserver> observers;

	public UnitSimple(String name, BehaviorSoldier behavior) {
		this.behavior = behavior;
		this.name = name;
		this.observers = new LinkedList<>();
	}

	@Override
	public String getName() {
		return name;
	}

	@Override
	public float getHealthPoints() {
		return behavior.getHealthPoints();
	}

	@Override
	public boolean alive() {
		return behavior.alive();
	}

	@Override
	public void heal() {
		behavior.heal();
	}

	@Override
	public float parry(float force) {
		float result = behavior.parry(force);
		notifyObservers();
		return result;
	}

	@Override
	public float strike() {
		float result = behavior.strike();
		return result;
	}

	@Override
	public void addEquipment(Equipment w) {
		behavior = w.createExtension(behavior);
	}

	@Override
	public void removeEquipment(Equipment w) {
		BehaviorSoldier current = behavior;
		BehaviorSoldier previous = behavior;
		while (current instanceof BehaviorExtension
				&& ((BehaviorExtension) current).getOwner() != w) {
			previous = current;
			current = ((BehaviorExtension) current).parent();
		}
		if (((BehaviorExtension) current).getOwner() == w) {
			((BehaviorExtension) previous)
					.reparent(((BehaviorExtension) previous).parent());
		}
	}

	@Override
	public Iterator<Equipment> getEquipments() {
		return new Iterator<Equipment>() {
			BehaviorSoldier current = behavior;

			@Override
			public boolean hasNext() {
				return current instanceof BehaviorExtension;
			}

			@Override
			public Equipment next() {
				Equipment tmp = ((BehaviorExtension) current).getOwner();
				current = ((BehaviorExtension) current).parent();
				return tmp;
			}
		};
	}

	@Override
	final public Iterator<Unit> subUnits() {
		return Collections.emptyIterator();
	}

	@Override
	final public void addUnit(Unit au) {
		throw new UnsupportedOperationException();
	}

	@Override
	final public void removeUnit(Unit au) {
		throw new UnsupportedOperationException();
	}

	public int nbWeapons() {
		int result = 0;
		for (Iterator<Equipment> it = getEquipments(); it.hasNext(); it.next())
			++result;
		return result;
	}

	@Override
	public void addObserver(UnitObserver observer) {
		if(!this.observers.contains(observer)) //Eviter les doublons
			this.observers.add(observer);
	}

	@Override
	public void removeObserver(UnitObserver observer) {
		this.observers.remove(observer);
	}

	@Override
	public void notifyObservers() {
		List<UnitObserver> cloneObs = new LinkedList<>(this.observers);

		for (UnitObserver o : cloneObs) {
			o.update(this);
		}
	}

	@Override
	public void accept(UnitVisitor u) {

	}
}
