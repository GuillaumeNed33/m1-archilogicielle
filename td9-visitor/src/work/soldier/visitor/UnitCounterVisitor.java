package work.soldier.visitor;

import work.soldier.core.*;

public class UnitCounterVisitor extends ConcreteUnitVisitor {
    private int nbSoldierInArmy = 0;

    public UnitCounterVisitor() {

    }

    @Override
    public void visit(UnitRider u) {
        nbSoldierInArmy++;
    }

    @Override
    public void visit(UnitInfantry u) {
        nbSoldierInArmy++;
    }

    public int getNbSoldier() {
        return this.nbSoldierInArmy;
    }
}
