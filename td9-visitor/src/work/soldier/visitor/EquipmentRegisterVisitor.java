package work.soldier.visitor;

import work.soldier.core.*;
import work.soldier.equipment.WeaponVisitor;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class EquipmentRegisterVisitor extends ConcreteUnitVisitor {
    private List<Equipment> equipments;

    public EquipmentRegisterVisitor() {
        this.equipments = new ArrayList<>();
    }

    public void printAllEquiments() {
        for(Equipment e : equipments) {
            System.out.println("\t- " + e.getName());
        }
    }
}
