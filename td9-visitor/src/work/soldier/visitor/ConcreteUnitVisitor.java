package work.soldier.visitor;

import work.soldier.core.UnitGroup;
import work.soldier.core.UnitInfantry;
import work.soldier.core.UnitRider;
import work.soldier.core.UnitVisitor;

public abstract class ConcreteUnitVisitor implements UnitVisitor {
    @Override
    public void visit(UnitRider u) {

    }

    @Override
    public void visit(UnitInfantry u) {

    }

    @Override
    public void visit(UnitGroup u) {
    }
}
