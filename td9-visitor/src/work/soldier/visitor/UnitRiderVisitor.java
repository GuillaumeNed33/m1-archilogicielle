package work.soldier.visitor;

import work.soldier.core.UnitGroup;
import work.soldier.core.UnitInfantry;
import work.soldier.core.UnitRider;
import work.soldier.core.UnitVisitor;

import java.util.HashSet;
import java.util.Set;

public class UnitRiderVisitor extends ConcreteUnitVisitor {
    private Set<UnitRider> unitRiders;
    private int health;

    public UnitRiderVisitor(int health) {
        unitRiders = new HashSet<>();
        this.health = health;
    }

    @Override
    public void visit(UnitRider u) {
        if(u.getHealthPoints() >= this.health) {
            unitRiders.add(u);
        }
    }

    public Set<UnitRider> getUnitRiderGoodHealth() {
        return new HashSet<>(this.unitRiders);
    }

}
