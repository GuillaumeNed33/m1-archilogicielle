package work.soldier.observer;

import work.soldier.core.Unit;
import work.soldier.core.UnitObserver;

public class KillObserver implements UnitObserver {
    public void update(Unit unit) {
        if(!unit.alive())
            System.out.println("==> KILL : L'unité : " + unit.getName() + " est morte.");
    }
}
