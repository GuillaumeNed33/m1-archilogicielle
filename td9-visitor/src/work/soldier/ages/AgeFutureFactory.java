/**
 * D. Auber & P. Narbel
 * Solution TD Architecture Logicielle 2016 Universit� Bordeaux.
 */
package work.soldier.ages;

import work.soldier.core.AgeAbstractFactory;
import work.soldier.core.Equipment;
import work.soldier.core.Unit;
import work.soldier.equipment.EquipmentTrumpet;
import work.soldier.equipment.WeaponGun;
import work.soldier.equipment.WeaponShield;
import work.soldier.units.UnitBikerMan;
import work.soldier.units.UnitRobot;

public class AgeFutureFactory implements AgeAbstractFactory {

	@Override
	public Unit infantryUnit(String name) {
		return new UnitRobot(name);
	}

	@Override
	public Unit riderUnit(String name) {
		return new UnitBikerMan(name);
	}

	@Override
	public Equipment attackWeapon() {
		return new WeaponGun();
	}

	@Override
	public Equipment defenseWeapon() {
		return new WeaponShield();
	}
	
	@Override
	public Equipment toy() {
		return new EquipmentTrumpet();
	}

}
