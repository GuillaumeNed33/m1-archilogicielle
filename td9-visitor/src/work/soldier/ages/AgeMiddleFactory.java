/**
 * D. Auber & P. Narbel
 * Solution TD Architecture Logicielle 2016 Universit� Bordeaux.
 */
package work.soldier.ages;

import work.soldier.core.AgeAbstractFactory;
import work.soldier.core.Equipment;
import work.soldier.core.Unit;
import work.soldier.equipment.EquipmentTrumpet;
import work.soldier.equipment.WeaponShield;
import work.soldier.equipment.WeaponSword;
import work.soldier.units.UnitCenturion;
import work.soldier.units.UnitHorseMan;

public class AgeMiddleFactory implements AgeAbstractFactory {

	@Override
	public Unit infantryUnit(String name) {
		return new UnitCenturion(name);
	}

	@Override
	public Unit riderUnit(String name) {
		return new UnitHorseMan(name);
	}

	@Override
	public Equipment attackWeapon() {
		return new WeaponSword();
	}

	@Override
	public Equipment defenseWeapon() {
		return new WeaponShield();
	}
	
	@Override
	public Equipment toy() {
		return new EquipmentTrumpet();
	}
}
