/**
 * D. Auber & P. Narbel
 * Solution TD Architecture Logicielle 2016 Universit� Bordeaux.
 */
package work.soldier.equipment;

import work.soldier.core.EquipmentAttack;
import work.soldier.core.EquipmentDefense;
import work.soldier.core.EquipmentToy;

public interface WeaponVisitor {
	void visit(EquipmentAttack s);
	void visit(EquipmentDefense s);
	void visit(EquipmentToy s);
}
