package work;

public class AlgorithmWithCache implements AlgorithmFactory {
    @Override
    public Algorithm getFibonacci() {
        return new AlgorithmCache(new Fibonacci());
    }

    @Override
    public Algorithm getPadovan() {
        return new AlgorithmCache(new Padovan());
    }
}
