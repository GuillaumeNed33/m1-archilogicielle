package work;

public interface AlgorithmFactory {
    Algorithm getFibonacci();
    Algorithm getPadovan();
}
