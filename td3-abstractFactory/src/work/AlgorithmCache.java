package work;

import java.util.HashMap;

public class AlgorithmCache implements Algorithm {
    private Algorithm algo;
    private HashMap<Integer, Double> cache;

    public AlgorithmCache(Algorithm algo) {
        this.algo = algo;
        cache = new HashMap<>();
    }

    @Override
    public String getName() {
        return algo.getName();
    }

    @Override
    public double getVal(int i) {
        if(!cache.containsKey(i)) {
            cache.put(i, algo.getVal(i));
        }
        return cache.get(i);
    }
}
