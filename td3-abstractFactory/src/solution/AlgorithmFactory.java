package solution;

public interface AlgorithmFactory {
	Algorithm getFibonacci();
	Algorithm getPadovan();	
}
