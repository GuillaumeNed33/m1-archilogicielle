import org.junit.Test;
import work.*;

import java.util.Scanner;

import static junit.framework.TestCase.assertEquals;

public class AppTest {

    @Test
    public void TestCache() {
        Algorithm fibonacci = new Fibonacci();
        Algorithm padovan =  new Padovan();
        Algorithm cachedFibo = new AlgorithmCache(new Fibonacci());
        Algorithm cachedPadovan = new AlgorithmCache(new Padovan());

        for(int i= 0 ; i < 10; i++) {
            assertEquals(fibonacci.getVal(i), cachedFibo.getVal(i));
            assertEquals(padovan.getVal(i), cachedPadovan.getVal(i));
        }
    }

    public static void main(String argv[]) {
        if (argv.length == 0)
            throw new Error("No parameters detected.");

        System.out.println("Paramètres : " + argv[0] + " " + Integer.toString(Integer.parseInt(argv[1])));
        AlgorithmFactory factory = null;

        if (argv[0].equals("ClassicAlgorithm"))
            factory = new ClassicAlgorithm();
        else if (argv[0].equals("AlgorithmWithCache"))
            factory = new AlgorithmWithCache();
        else
            throw new Error("Nom de la classe concrète incorrect.");

        Algorithm fibonacci = factory.getFibonacci();
        Algorithm padovan = factory.getPadovan();
        System.out.println("Résultat de Fibonacci avec en entrée " + argv[1] + " : " + fibonacci.getVal(Integer.parseInt(argv[1])));
        System.out.println("Résultat de Padovan avec en entrée " + argv[1] + " : " + padovan.getVal(Integer.parseInt(argv[1])));
    }
}
