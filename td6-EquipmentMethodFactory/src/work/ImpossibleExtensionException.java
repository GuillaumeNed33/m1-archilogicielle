package work;

public class ImpossibleExtensionException extends RuntimeException {
    public ImpossibleExtensionException() {
        super();
    }

    public ImpossibleExtensionException(String message) {
        super(message);
    }

    public ImpossibleExtensionException(String message, Throwable cause) {
        super(message, cause);
    }

    public ImpossibleExtensionException(Throwable cause) {
        super(cause);
    }

    protected ImpossibleExtensionException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
