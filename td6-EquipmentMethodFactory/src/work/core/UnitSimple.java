package work.core;

import work.weapon.Weapon;

public abstract class UnitSimple {
    private String m_name;
    private BehaviorSoldier soldier;

    public UnitSimple(String name, BehaviorSoldier extension) {
        this.soldier = extension;
        this.m_name = name;
    }

    public float parry(float force) {
        return soldier.parry(force);
    }

    public float strike() {
        return soldier.strike();
    }

    public String getName() {
        return this.m_name;
    }

    public void setName(String name) {
        this.m_name = name;
    }

    public float getHealthPoints() {
        return soldier.getHealthPoints();
    }

    public boolean alive() {
        return soldier.alive();
    }

    public void heal() {
        soldier.heal();
    }

    public void addWeapon(Weapon w) {
        soldier = w.createExtension(soldier);
    }

    public void removeWeapon(Weapon w) {

    }
}
