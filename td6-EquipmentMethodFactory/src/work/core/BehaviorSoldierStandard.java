package work.core;

public class BehaviorSoldierStandard implements BehaviorSoldier {
    private float m_attack;
    private float m_defense;
    private float m_health;

    BehaviorSoldierStandard(float attack, float defense, float hp) {
        this.m_attack = attack;
        this.m_defense = defense;
        this.m_health = hp;
    }

    public String getName() {
        return null;
    }

    @Override
    public float getHealthPoints() {
        return m_health;
    }

    @Override
    public boolean alive() {
        return getHealthPoints() > 0;
    }

    @Override
    public void heal() {
        m_health += 10;
    }

    @Override
    public float parry(float force) {
        float strike = (float)Math.sqrt(force / m_defense + 1);
        m_health -= strike;

        if(!alive())
            return (float)-m_health;
        return 0.f;
    }

    public float strike() {
        return m_attack * (float)Math.log10(m_health + 1.);
    }

}
