package work.core;
import java.lang.Math;

public class LogPowBehavior extends UnitSimple {

    public LogPowBehavior(float attack, float defense, float hp, String name) {
    	super(name, new BehaviorSoldierStandard(attack, defense, hp));
    }
}
