package work.core;

public class ConstBehavior extends UnitSimple {
	public ConstBehavior(float attack, float defense, float hp, String name) {
		super(name, new BehaviorSoldierStandard(attack, defense, hp));
	}
}
