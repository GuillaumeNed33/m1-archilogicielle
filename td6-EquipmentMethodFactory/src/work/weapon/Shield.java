package work.weapon;

import work.core.BehaviorSoldier;
import work.core.StdExtension;

public class Shield extends Weapon {
    @Override
    public BehaviorSoldier createExtension(BehaviorSoldier s) {
        return new StdExtension(5, 60, s);
    }
}
