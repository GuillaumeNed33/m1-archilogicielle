package work.weapon;

import work.core.BehaviorSoldier;
import work.core.StdExtension;

public class Sword extends Weapon {
    @Override
    public BehaviorSoldier createExtension(BehaviorSoldier s) {
        return new StdExtension(50, 10, s);
    }
}
