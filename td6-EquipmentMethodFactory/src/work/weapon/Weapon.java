package work.weapon;

import work.core.BehaviorSoldier;

public abstract class Weapon  {
    public abstract BehaviorSoldier createExtension(BehaviorSoldier s);
}
