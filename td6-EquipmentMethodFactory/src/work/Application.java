package work;

import org.junit.Test;
import work.core.ConstBehavior;
import work.core.LogPowBehavior;
import work.core.UnitSimple;
import work.weapon.Shield;
import work.weapon.Sword;
import work.weapon.Weapon;

public class Application {

    @Test
    public void fight() {
        UnitSimple hm = new LogPowBehavior(50, 10, 300, "Horseman");
        UnitSimple im = new ConstBehavior(50, 10, 300, "InfantryMan");
        im.parry(hm.strike());
        /*try {
            im.addSword();
            hm.addShield();
            hm.parry(im.strike());
            im.addShield();
            im.addSword(); // Lève une exception car deux armes maximum
        } catch(ImpossibleExtensionException e) {}*/
    }

    @Test
    public void fightv2() {
        UnitSimple hm = new LogPowBehavior(50, 10, 300, "Horseman");
        UnitSimple im = new ConstBehavior(60, 3, 300, "InfantryMan");
        Weapon sw = new Sword();
        Weapon sh = new Shield();
        Weapon sw2 = new Sword();
        try {
            im.parry(hm.strike());
            im.addWeapon(sw);
            hm.addWeapon(sw); // Lève une exception car l'arme est déjà attaché
        }catch(ImpossibleExtensionException e) {}

        try {
            im.removeWeapon(sw);
            hm.addWeapon(sw);
            hm.addWeapon(sh);
            hm.addWeapon(sw2); // Lève une exception car deux armes maximum
        }catch(ImpossibleExtensionException e) {}
    }
}
