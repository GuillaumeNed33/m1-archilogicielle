package solution; /**
 * D. Auber & P. Narbel
 * Solution TD Architecture Logicielle 2016 Universit� Bordeaux.
 */


import solution.soldier.core.Equipment;
import solution.soldier.core.EquipmentException;
import solution.soldier.core.UnitSimple;
import solution.soldier.equipment.EquipmentTrumpet;
import solution.soldier.equipment.WeaponShield;
import solution.soldier.equipment.WeaponSword;
import solution.soldier.units.UnitCenturion;
import solution.soldier.units.UnitRobot;

public class MainFightExemple {
	
	public static void main12(String[] args) {

		UnitSimple trooper = new UnitCenturion("StormTrooper");
		UnitSimple r2 = new UnitRobot("R2D2");
		
		trooper.addEquipment(new WeaponSword());
		trooper.addEquipment(new WeaponShield());
		
		r2.addEquipment(new WeaponShield());
		r2.addEquipment(new EquipmentTrumpet());
		
		int round = 0;
		while(trooper.alive() && r2.alive()) {
			System.out.println("Round  #" + round++);
			float st1 = trooper.strike();
			System.out.println(trooper.getName() + " attack with force : " + st1);
			r2.parry(st1);
			if (r2.alive()) {
				float st2 = r2.strike();
				System.out.println(r2.getName() + " attack with force : " + st2);
				trooper.parry(st2);
			}
		}
		System.out.println("The end ... " + (trooper.alive() ? trooper.getName() : r2.getName()) + " won." );
	}

	public static void main(String[] args) {
		UnitSimple hm = new UnitCenturion("StormTrooper");
		UnitSimple im = new UnitRobot("R2D2");
		Equipment sw = new WeaponSword();
		Equipment sh = new WeaponShield();
		Equipment sw2 = new WeaponSword();
		try {
			im.parry(hm.strike());
			im.addEquipment(sw);
			hm.addEquipment(sw); // Lève une exception car l'arme est déjà attaché
		}catch(EquipmentException e) {}

		try {
			im.removeEquipment(sw);
			hm.addEquipment(sw);
			hm.addEquipment(sh);
			hm.addEquipment(sw2); // Lève une exception car deux armes maximum
		}catch(EquipmentException e) {}
	}

}
