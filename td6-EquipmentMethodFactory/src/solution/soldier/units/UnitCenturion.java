/**
 * D. Auber & P. Narbel
 * Solution TD Architecture Logicielle 2016 Universit� Bordeaux.
 */
package solution.soldier.units;

import solution.soldier.core.EquipmentException;
import solution.soldier.core.UnitInfantry;
import solution.soldier.core.Equipment;

public class UnitCenturion extends UnitInfantry {

	public UnitCenturion(String soldierName) {
		super(soldierName, new BehaviorSoldierStd(200, 50));
	}

	/**
	 * A Centurion can have at most two equipments
	 */
	@Override
	public void addEquipment(Equipment w) {
		if (nbWeapons() > 1)
			throw new EquipmentException();
		super.addEquipment(w);
	}

}
