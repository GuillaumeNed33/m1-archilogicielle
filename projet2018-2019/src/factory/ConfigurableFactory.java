package factory;

import core.Brick;

public class ConfigurableFactory implements LEGOFactory {
    private Brick b1x1;
    private Brick b1x2;
    private Brick b1x4;
    private Brick b2x2;
    private Brick b2x4;

    public ConfigurableFactory(Brick b1x1, Brick b1x2, Brick b1x4, Brick b2x2, Brick b2x4) {
        this.b1x1 = b1x1;
        this.b1x2 = b1x2;
        this.b1x4 = b1x4;
        this.b2x2 = b2x2;
        this.b2x4 = b2x4;
    }

    @Override
    public Brick createBrick1x1() {
        return b1x1.clone();
    }

    @Override
    public Brick createBrick1x2() {
        return b1x2.clone();
    }

    @Override
    public Brick createBrick1x4() {
        return b1x4.clone();
    }

    @Override
    public Brick createBrick2x2() {
        return b2x2.clone();
    }

    @Override
    public Brick createBrick2x4() {
        return b2x4.clone();
    }
}
