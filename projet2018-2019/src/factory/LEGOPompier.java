package factory;

import bricks.*;
import core.*;

/**
 * Factory de Bricks rouges
 */
public class LEGOPompier implements LEGOFactory {
    private int color = 0xFF0000;

    @Override
    public Brick createBrick1x1() {
        return new Brick1x1(color);
    }

    @Override
    public Brick createBrick1x2() {
        return new Brick1x2(color);
    }

    @Override
    public Brick createBrick1x4() {
        return new Brick1x4(color);
    }

    @Override
    public Brick createBrick2x2() {
        return new Brick2x2(color);
    }

    @Override
    public Brick createBrick2x4() {
        return new Brick2x4(color);
    }
}
