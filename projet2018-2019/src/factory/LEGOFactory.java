package factory;

import core.Brick;

/**
 * Abstract Factory
 */
public interface LEGOFactory {
    Brick createBrick1x1();
    Brick createBrick1x2();
    Brick createBrick1x4();
    Brick createBrick2x2();
    Brick createBrick2x4();
}
