import bricks.*;
import builder.LEGOBuilder;
import builder.LEGOBuiltCounter;
import builder.LEGOOrientableBrickBuilder;
import core.Brick;
import core.OrientableBrick;
import core.Orientation;
import factory.ConfigurableFactory;
import factory.LEGOFactory;
import factory.LEGOPolicier;
import factory.LEGOPompier;
import org.junit.Test;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;


public class AppTest {

    //@Test
    public static void testAbstractFactory() {
        LEGOFactory pompier = new LEGOPompier();
        LEGOFactory policier = new LEGOPolicier();
        LEGOFactory params = new ConfigurableFactory(
                new Brick1x1(0x55F0),
                new Brick1x2(0x55F0),
                new Brick1x4(0x55F0),
                new Brick2x2(0x55F0),
                new Brick2x4(0x55F0)
        );
        Set<Brick> bricks = new HashSet<>();

        for(int i = 0; i < 50 ; i++) {
            double random = Math.random();
            if (random <= 0.33f) {
                bricks.add(policier.createBrick1x1());
            }
            else if (random > 0.33f && random <= 0.66f) {
                bricks.add(pompier.createBrick1x1());
            }
            else {
                bricks.add(params.createBrick1x1());
            }
        }
        for(Brick b : bricks) {
            System.out.println(b.toString());
        }
    }

    //@Test
    public static void testBuilderCounter() {
        LEGOBuilder counter = new LEGOBuiltCounter();
        for(int i = 0; i < 50 ; i++) {
            int random = (int) (Math.random() * 5);
            switch (random) {
                case 0 :
                    counter.get1x1();
                    break;
                case 1 :
                    counter.get1x2();
                    break;
                case 2 :
                    counter.get1x4();
                    break;
                case 3 :
                    counter.get2x2();
                    break;
                case 4 :
                    counter.get2x4();
                    break;
                default:
                    break;
            }
        }
        System.out.println(((LEGOBuiltCounter) counter).displayResults());
    }

    //@Test
    public static void testBuilderQuestion9() {
        LEGOFactory factory = new ConfigurableFactory(
                new Brick1x1(0x582900), // 1x1 marron
                new Brick1x2(0xFFFF00), // 1x2 jaune
                new Brick1x4(0xFFFF00), // 1x4 jaune
                new Brick2x2(0xFF0000), // 2x2 rouge
                new Brick2x4(0xFF0000) // 2x4 rouge
        );
        LEGOOrientableBrickBuilder builder = new LEGOOrientableBrickBuilder(factory);
        //Positionnement de la première pièce
        builder.get2x4();
        builder.rotate(Orientation.EAST);
        builder.moveTo(0, 0);
        builder.drop();

        //Positionnement de la seconde pièce
        builder.get1x2();
        builder.moveTo(0, 1);
        builder.drop();

        //Positionnement de la troisième pièce
        builder.get1x4();
        builder.rotate(Orientation.EAST);
        builder.moveTo(0, 0);
        builder.drop();

        //Positionnement de la quatrième pièce
        builder.get1x1();
        builder.moveTo(3, 0);
        builder.drop();

        //Positionnement de la dernière pièce
        builder.get2x2();
        builder.moveTo(3, 1);
        builder.drop();

        System.out.println("Pièces de la grille :");
        System.out.println("-----------------------------------\n");

        for (OrientableBrick b : builder.getBricks()) {
            System.out.println(b.getBrick().toString());
            System.out.println(b.getPosition().toString());
            System.out.println("Orientation : " + b.getOrientation());

        }
        System.out.println("-----------------------------------\n\n");
        displayReliefGrid(builder);
    }

    public static void displayReliefGrid(LEGOOrientableBrickBuilder builder) {
        System.out.println("Grille relief : ");
        System.out.println("-----------------------------------\n");
        String res;
        for(int i = 0; i < builder.getGridSize(); i++) {
            res = "";
            for(int j = 0; j < builder.getGridSize(); j++) {
                res += builder.getReliefGrid()[j][i];
                res += "  ";
            }
            System.out.println(res);
        }
        System.out.println("-----------------------------------\n\n");
    }

    public static int choiceBrick() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Que voulez-vous faire ? (1, 2, 3 ou 4)\n" +
                "\t1 : Prendre une brique 1x1\n" +
                "\t2 : Prendre une brique 1x2\n" +
                "\t3 : Prendre une brique 1x4\n" +
                "\t4 : Prendre une brique 2x2\n" +
                "\t5 : Prendre une brique 2x4\n" +
                "\t6 : Quitter");
        int choice = sc.nextInt();
        while (choice != 1 && choice != 2 && choice != 3 && choice != 4 && choice != 5 && choice != 6) {
            System.out.println("Erreur : Choix indisponible");
            System.out.println("Que voulez-vous faire ? (1, 2, 3 ou 4)\n" +
                    "\t1 : Prendre une brique 1x1\n" +
                    "\t2 : Prendre une brique 1x2\n" +
                    "\t3 : Prendre une brique 1x4\n" +
                    "\t4 : Prendre une brique 2x2\n" +
                    "\t5 : Prendre une brique 2x4\n" +
                    "\t6 : Quitter");
            choice = sc.nextInt();
        }
        return choice;
    }
    public static int choiceMove() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Que voulez-vous faire ? (1, 2 ou 3)\n" +
                "\t1 : Modifier la position de la brique\n" +
                "\t2 : Modifier l'orientation de la brique\n" +
                "\t3 : Poser la brique");
        int choice = sc.nextInt();
        while (choice != 1 && choice != 2 && choice != 3) {
            System.out.println("Erreur : Choix indisponible");
            System.out.println("Que voulez-vous faire ? (1, 2 ou 3)\n" +
                    "\t1 : Modifier la position de la brique\n" +
                    "\t2 : Modifier l'orientation de la brique\n" +
                    "\t3 : Poser la brique");
            choice = sc.nextInt();
        }
        return choice;
    }
    public static int[] choicePosX_Y() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Donnez la position X\n");
        int[] choice = new int[2];
        choice[0] = sc.nextInt();
        System.out.println("Donnez la position Y\n");
        choice[1] = sc.nextInt();
        return choice;
    }
    public static int choiceOrientation() {
        Scanner sc = new Scanner(System.in);
        System.out.println("Choix de l'orientation ? (1, 2, 3 ou 4)\n" +
                "\t1 : Orienter vers le NORD\n" +
                "\t2 : Orienter vers le SUD\n" +
                "\t3 : Orienter vers l'EST\n" +
                "\t4 : Orienter vers l'OUEST ");
        int choice = sc.nextInt();
        while (choice != 1 && choice != 2 && choice != 3 && choice != 4) {
            System.out.println("Erreur : Choix indisponible");
            System.out.println("Choix de l'orientation ? (1, 2, 3 ou 4)\n" +
                    "\t1 : Orienter vers le NORD\n" +
                    "\t2 : Orienter vers le SUD\n" +
                    "\t2 : Orienter vers l'EST\n" +
                    "\t3 : Orienter vers l'OUEST ");
            choice = sc.nextInt();
        }
        return choice;
    }

    public static void customBuilding() {
        LEGOFactory factory = new ConfigurableFactory(
                new Brick1x1(0x582900), // 1x1 marron
                new Brick1x2(0xFFFF00), // 1x2 jaune
                new Brick1x4(0xFFFF00), // 1x4 jaune
                new Brick2x2(0xFF0000), // 2x2 rouge
                new Brick2x4(0xFF0000) // 2x4 rouge
        );
        LEGOOrientableBrickBuilder builder = new LEGOOrientableBrickBuilder(factory);

        int choice = choiceBrick();
        while (choice != 6) {
            switch (choice) {
                case 1:
                    builder.get1x1();
                    break;
                case 2:
                    builder.get1x2();
                    break;
                case 3:
                    builder.get1x4();
                    break;
                case 4:
                    builder.get2x2();
                    break;
                case 5:
                    builder.get2x4();
                    break;
                default:
                    break;
            }

            boolean movePiece = true;
            while (movePiece) {
                switch (choiceMove()) {
                    case 1:
                        int[] posXY = choicePosX_Y();
                        try {
                            builder.moveTo(posXY[0], posXY[1]);
                        } catch (Exception e) {
                            System.out.println("La pièce est en dehors de la grille avec cette position. Opération annulée.");
                            movePiece = true;
                        }
                        break;
                    case 2:
                        int rotation = choiceOrientation();
                        try {
                            switch (rotation) {
                                case 1:
                                    builder.rotate(Orientation.NORTH);
                                    break;
                                case 2:
                                    builder.rotate(Orientation.SOUTH);
                                    break;
                                case 3:
                                    builder.rotate(Orientation.EAST);
                                    break;
                                case 4:
                                    builder.rotate(Orientation.WEST);
                                    break;
                            }
                        } catch (Exception e) {
                            System.out.println("La pièce est en dehors de la grille avec cette rotation. Opération annulée.");
                            movePiece = true;
                        }
                        break;
                    case 3:
                        try {
                            builder.drop();
                            movePiece = false;
                            displayReliefGrid(builder);
                        } catch (Exception e) {
                            System.out.println("La pièce est en dehors de la grille. Modifier sa position/orientation pour pouvoir la poser.");
                            movePiece = true;
                        }
                        break;
                    default:
                        break;
                }
            }
            choice = choiceBrick();
        }

    }

    public static void main(String argv[]) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Quel test voulez-vous lancer ? (1, 2, 3 ou 4)\n" +
                "\t1 : Test des fabriques\n" +
                "\t2 : Test du builder compteur\n" +
                "\t3 : Test du builder de briques Orientées (Q9)\n" +
                "\t4 : Construction personnalisée");
        int choice = sc.nextInt();
        while (choice != 1 && choice != 2 && choice != 3 && choice != 4) {
            System.out.println("Erreur votre choix doit être 1, 2 ou 3.");
            System.out.println("Quel test voulez-vous lancer ? (1, 2, 3 ou 4)\n" +
                    "\t1 : Test des fabriques\n" +
                    "\t2 : Test du builder compteur\n" +
                    "\t3 : Test du builder de briques Orientées (Q9)\n" +
                    "\t4 : Construction personnalisée");
            choice = sc.nextInt();
        }
        if(choice == 1) {
            testAbstractFactory();
        }
        else if(choice == 2) {
            testBuilderCounter();
        }
        else if(choice == 3) {
            testBuilderQuestion9();
        }
        else {
            customBuilding();
        }
    }
}
