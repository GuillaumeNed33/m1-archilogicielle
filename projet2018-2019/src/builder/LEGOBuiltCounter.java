package builder;

import core.Orientation;

public class LEGOBuiltCounter implements LEGOBuilder {
    private int[] result = new int[5];

    public LEGOBuiltCounter() {
        result[0] = 0;
        result[1] = 0;
        result[2] = 0;
        result[3] = 0;
        result[4] = 0;
    }

    @Override
    public void get1x1() {
        result[0]++;
    }

    @Override
    public void get1x2() {
        result[1]++;
    }

    @Override
    public void get1x4() {
        result[2]++;
    }

    @Override
    public void get2x2() {
        result[3]++;
    }

    @Override
    public void get2x4() {
        result[4]++;
    }

    @Override
    public void moveTo(int x, int y) {
        //Do nothing
    }

    @Override
    public void rotate(Orientation orientation) {
        //Do nothing
    }

    @Override
    public void drop() {
        //Do nothing
    }

    public int[] getResults() {
        return result;
    }

    public String displayResults() {
        return "- 1x1 bricks built : " + result[0] + "\n" +
        "- 1x2 bricks built : " + result[1] + "\n" +
        "- 1x4 bricks built : " + result[2] + "\n" +
        "- 2x2 bricks built : " + result[3] + "\n" +
        "- 2x4 bricks built : " + result[4] + "\n";
    }
}
