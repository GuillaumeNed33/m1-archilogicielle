package builder;

import core.OrientableBrick;
import core.Orientation;
import factory.LEGOFactory;

import java.util.ArrayList;
import java.util.List;

public class LEGOOrientableBrickBuilder implements LEGOBuilder {

    private final LEGOFactory factory;
    private final int SIZE = 10;
    private int reliefGrid[][] = new int[SIZE][SIZE];
    private List<OrientableBrick> bricks;
    private OrientableBrick currentBrick;

    public LEGOOrientableBrickBuilder(LEGOFactory factory) {
        this.factory = factory;
        bricks = new ArrayList<>();
        currentBrick = null;
        //Relief du plateau à 0 (aucune briques posées)
        for(int i = 0; i < SIZE; i++) {
            for(int j = 0; j < SIZE; j++) {
                reliefGrid[i][j] = 0;
            }
        }
    }

    @Override
    public void get1x1() {
        this.currentBrick = new OrientableBrick(factory.createBrick1x1());
    }

    @Override
    public void get1x2() {
        this.currentBrick = new OrientableBrick(factory.createBrick1x2());
    }

    @Override
    public void get1x4() {
        this.currentBrick = new OrientableBrick(factory.createBrick1x4());
    }

    @Override
    public void get2x2() {
        this.currentBrick = new OrientableBrick(factory.createBrick2x2());
    }

    @Override
    public void get2x4() {
        this.currentBrick = new OrientableBrick(factory.createBrick2x4());
    }

    @Override
    public void moveTo(int x, int y) {
        if(verifyPosition(x, y, currentBrick.getOrientation())) {
            this.currentBrick.getPosition().setX(x);
            this.currentBrick.getPosition().setY(y);
        } else {
            throw new RuntimeException("the brick is out of the grid at this position.");
        }
    }

    @Override
    public void rotate(Orientation orientation) {
        if(verifyPosition(currentBrick.getPosition().getX(), currentBrick.getPosition().getY(), orientation)) {
            this.currentBrick.setOrientation(orientation);
        } else {
            throw new RuntimeException("the brick is out of the grid with this orientation.");
        }
    }

    @Override
    public void drop() {
        if(verifyPosition(currentBrick.getPosition().getX(), currentBrick.getPosition().getY(), currentBrick.getOrientation())) {
            int[] data = calculatePos(
                    currentBrick.getPosition().getX(),
                    currentBrick.getPosition().getY(),
                    currentBrick.getOrientation()
            );
            int x = data[0];
            int y = data[1];
            int width = data[2];
            int height =data[3];

            //Recherche de la hauteur max existante (ou va être placée la pièce) => des morceaux de la pièce peuvent "voler"
            int max = 0;
            for (int i = x; i < x + width; i++) {
                for (int j = y; j < y + height; j++) {
                    if (reliefGrid[i][j] > max) {
                        max = reliefGrid[i][j];
                    }
                }
            }
            //Mise à jour du relief
            for (int i = x; i < x + width; i++) {
                for (int j = y; j < y + height; j++) {
                        reliefGrid[i][j] = max + 1;
                }
            }

            bricks.add(this.currentBrick);
            currentBrick = null;
        }
        else {
            throw new RuntimeException("the brick is out of the grid.");
        }
    }

    /**
     * Vérifie que la brique se trouve dans le plateau de jeu avec sa position et son orientation
     */
    public boolean verifyPosition(int x, int y, Orientation orientation) {
        int[] data = calculatePos(x, y, orientation);
        int posX = data[0];
        int posY = data[1];
        int width = data[2];
        int height =data[3];
        if(posX >= 0 && posX < SIZE
                && posX + width >= 0 && posX + width < SIZE
                && posY >= 0 && posY < SIZE
                && posY + height >= 0 && posY + height < SIZE) {
            return true;
        } else {
            return false;
        }
    }

    private int[] calculatePos(int x, int y, Orientation orientation) {
        int [] res = new int[4];
        switch (orientation) {
            case NORTH:
                res[0] = x;
                res[1] = y - (currentBrick.getBrick().getHeight() -1);
                res[2] = currentBrick.getBrick().getWidth();
                res[3] = currentBrick.getBrick().getHeight();
                break;
            case SOUTH:
                res[0] = x - (currentBrick.getBrick().getWidth() -1);
                res[1] = y;
                res[2] = currentBrick.getBrick().getWidth();
                res[3] = currentBrick.getBrick().getHeight();
                break;
            case EAST:
                res[0] = x;
                res[1] = y;
                res[2] = currentBrick.getBrick().getHeight();
                res[3] = currentBrick.getBrick().getWidth();
                break;
            case WEST:
                res[0] = x - (currentBrick.getBrick().getHeight() -1);
                res[1] = y - (currentBrick.getBrick().getWidth() -1);
                res[2] = currentBrick.getBrick().getHeight();
                res[3] = currentBrick.getBrick().getWidth();
                break;
        }
        return res;
    }

    public List<OrientableBrick> getBricks() {
        return bricks;
    }

    public int getGridSize() {
        return SIZE;
    }

    public int[][] getReliefGrid() {
        return reliefGrid;
    }
}
