package core;

public enum Orientation {
    EAST,
    WEST,
    NORTH,
    SOUTH
}
