package core;

/**
 * Classe abstraite permettant de factoriser le code des Bricks
 */
public abstract class AbstractBrick implements Brick, Cloneable {
    private int width;
    private int height;
    private int color;

    public AbstractBrick(int width, int height, int color) {
        this.width = width;
        this.height = height;
        this.color = color;
    }

    @Override
    public int getWidth() {
        return width;
    }

    @Override
    public int getHeight() {
        return height;
    }

    @Override
    public int getColor() {
        return color;
    }

    public String toString() {
        return "[width : " + width + ", height : " + height + ", color : " +  color + "]";
    }

    @Override
    public Brick clone() {
        Brick b = null;
        try {
            b = (Brick) super.clone();
        } catch(CloneNotSupportedException cnse) {
            cnse.printStackTrace(System.err);
        }
        return b;
    }
}
