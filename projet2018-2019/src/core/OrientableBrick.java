package core;

public class OrientableBrick {
    private Brick brick;
    private Orientation orientation;
    private Position position;

    public OrientableBrick(Brick b) {
        this.brick = b;
        this.position = new Position(0,0,0);
        this.orientation = Orientation.NORTH;
    }

    public Brick getBrick() {
        return brick.clone();
    }

    public Orientation getOrientation() {
        return orientation;
    }

    public void setOrientation(Orientation o) {
        this.orientation = o;
    }

    public Position getPosition() {
        return position;
    }
}
