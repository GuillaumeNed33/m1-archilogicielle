package bricks;

import core.AbstractBrick;

public class Brick1x1 extends AbstractBrick {
    public Brick1x1(int color) {
        super(1,1, color);
    }
}
